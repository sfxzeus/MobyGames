﻿using System;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "screenshot")]
    public class Screenshot
    {

        [DataMember(Name = "caption")]
        public string Caption { get; set; }

        [DataMember(Name = "height")]
        public int Height { get; set; }

        [DataMember(Name = "image")]
        public Uri Image { get; set; }

        [DataMember(Name = "thumbnail_image")]
        public Uri ThumbnailImage { get; set; }

        [DataMember(Name = "width")]
        public int Width { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}x{2} : {3}", Caption, Width, Height, Image);
        }
    }
}

