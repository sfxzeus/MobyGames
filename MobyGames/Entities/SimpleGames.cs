﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "games")]
    public class SimpleGames
    {
        [DataMember(Name = "games")]
        public int[] Games { get; set; }
    }
}
