﻿using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "rating")]
    public class GameRating
    {
        [DataMember(Name = "rating_id")]
        public int Id { get; set; }

        [DataMember(Name = "rating_name")]
        public string Name { get; set; }

        [DataMember(Name = "rating_system_name")]
        public string SystemName { get; set; }

        [DataMember(Name = "rating_system_id")]
        public int SystemId { get; set; }
    }
}
