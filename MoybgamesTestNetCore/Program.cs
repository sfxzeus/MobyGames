﻿using System;
using MobyGames;
using MobyGames.Entities;
using System.ComponentModel;


namespace MoybgamesTestNetCore
{
    class Program
    {
        static void Main(string[] args)
        {
            MobyGamesClient mg = new MobyGamesClient(System.IO.File.ReadAllText(AppContext.BaseDirectory + "\\key.txt"));
            /*Console.WriteLine("---------- Genres ----------");
            foreach (var genre in mg.Genres())
            {
                Console.WriteLine("Id : {0}, Name : {1}, CategoryId : {2},  Category : {3}", genre.Id, genre.Name, genre.CategoryId, genre.Category);
            }

            Console.WriteLine("---------- Groups ----------");
            foreach (var group in mg.Groups())
            {
                Console.WriteLine("Id : {0}, Name : {1}, Description : {2}", group.Id, group.Name, group.Description);
            }

            Console.WriteLine("---------- Platforms ----------");
            foreach (var platform in mg.Platforms())
            {
                Console.WriteLine("Id : {0}, Name : {1}", platform.Id, platform.Name );
            }*/

            Console.WriteLine("---------- Recent Games ID ----------");
            foreach (var game in mg.RecentGames(GameFormat.id))
            {
                Console.Write(game.Id + ",");
            }
            Console.WriteLine();
            Console.WriteLine("---------- Recent Games Brief ----------");
            foreach (var game in mg.RecentGames(GameFormat.brief, 21, 0, 10))
            {
                Console.WriteLine("Id : {0}, Title : {1}, Url :{2}", game.Id, game.Title, game.Url);
            }

            Console.ReadLine();
        }
    }
}