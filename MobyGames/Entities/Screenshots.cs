﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace MobyGames.Entities
{
    [DataContract(Name = "screenshots")]
    public class Screenshots : IEnumerable<Screenshot>
    {
        [DataMember(Name = "screenshots")]
        private List<Screenshot> _screenshots { get; set; }

        public Screenshot this[int index]
        {
            get { return _screenshots[index]; }
            set { _screenshots.Insert(index, value); }
        }

        public int Count { get { return _screenshots.Count; } }

        public IEnumerator<Screenshot> GetEnumerator()
        {
            return _screenshots.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }


    }
}

