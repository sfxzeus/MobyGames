﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "release")]
    public class GameRelease
    {
        [DataMember(Name = "companies")]
        public List<GameCompany> Companies{ get; set; }

        [DataMember(Name = "countries")]
        public List<string> Countries { get; set; }

        [DataMember(Name = "description")]
        public string Description;

        [DataMember(Name = "product_codes")]
        public List<ProductCode> ProductCodes;

        [DataMember(Name = "release_date")]
        private string _releaseDate;
        public DateTime ReleaseDate
        {
            get
            {
                if (_releaseDate.Length == 4)
                {
                    return new DateTime(int.Parse(_releaseDate), 1, 1);
                }
                else
                {
                    return DateTime.Parse(_releaseDate);
                }
            }
        }
        public GameRelease()
        {
            _releaseDate = string.Empty;

        }

    }
}
