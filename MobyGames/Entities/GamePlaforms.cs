﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "platforms")]
    public class GamePlatforms : IEnumerable<GamePlatform>
    {
        [DataMember(Name = "platforms")]
        private List<GamePlatform> _platforms { get; set; }

        public GamePlatform this[int index]
        {
            get { return _platforms[index]; }
            set { _platforms.Insert(index, value); }
        }

        public int Count { get { return _platforms.Count; } }

        public IEnumerator<GamePlatform> GetEnumerator()
        {
            return _platforms.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

    }
}
