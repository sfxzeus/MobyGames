﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "cover_group")]
    public class CoverGroup
    {
        [DataMember(Name = "comments")]
        public string Comments { get; set; }

        [DataMember(Name = "countries")]
        public List<string> Countries { get; set; }

        [DataMember(Name = "covers")]
        public List<Cover> Covers { get; set; }
    }
}
