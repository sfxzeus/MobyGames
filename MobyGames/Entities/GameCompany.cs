﻿using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "rating")]
    public class GameCompany
    {
        [DataMember(Name = "company_id")]
        public int Id { get; set; }

        [DataMember(Name = "company_name")]
        public string CompanyName { get; set; }

        [DataMember(Name = "role")]
        public string Role { get; set; }
    }
}
