﻿using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "product_code")]
    public class ProductCode
    {
        [DataMember(Name = "product_code_type_id")]
        public int Id { get; set; }

        [DataMember(Name = "product_code_type")]
        public string CodeType { get; set; }

        [DataMember(Name = "product_code")]
        public string Code { get; set; }
    }
}
