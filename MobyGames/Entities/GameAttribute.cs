﻿using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "attribute")]
    public class GameAttribute
    {
        [DataMember(Name = "attribute_id")]
        public int Id { get; set; }

        [DataMember(Name = "attribute_name")]
        public string Name { get; set; }

        [DataMember(Name = "attribute_category_name")]
        public string Category { get; set; }

        [DataMember(Name = "attribute_category_id")]
        public int CategoryId { get; set; }
    }
}
