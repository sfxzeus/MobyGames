﻿using System;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "patch")]
    public class Patch
    {
        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "release_date")]
        private string _releaseDate;
        public DateTime ReleaseDate
        {
            get
            {
                if (_releaseDate.Length == 4)
                {
                    return new DateTime(int.Parse(_releaseDate), 1, 1);
                }
                else
                {
                    return DateTime.Parse(_releaseDate);
                }
            }
        }
        public Patch()
        {
            _releaseDate = string.Empty;
        }
    }
}
