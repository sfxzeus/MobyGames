﻿using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "genre")]
    public class Genre
    {
        [DataMember(Name = "genre_id")]
        public int Id { get; set; }

        [DataMember(Name = "genre_name")]
        public string Name { get; set; }

        [DataMember(Name = "genre_description")]
        public string Description { get; set; }

        [DataMember(Name = "genre_category")]
        public string Category { get; set; }

        [DataMember(Name = "genre_category_id")]
        public int CategoryId { get; set; }
    }
}
