﻿using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "group")]
    public class Group
    {
        [DataMember(Name = "group_id")]
        public int Id { get; set; }

        [DataMember(Name = "group_name")]
        public string Name { get; set; }

        [DataMember(Name = "group_description")]
        public string Description { get; set; }
        
    }
}
