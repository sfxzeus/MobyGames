﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "GameInformation")]
    public class GameInformation
    {
        [DataMember(Name = "game_id")]
        public int GameId { get; set; }

        [DataMember(Name = "first_release_date")]
        private string _first_release_date;

        public DateTime FirstReleaseDate
        {
            get
            {
                if (_first_release_date.Length == 4)
                {
                    return new DateTime(int.Parse(_first_release_date), 1, 1);
                }
                else
                {
                    return DateTime.Parse(_first_release_date);
                }
            }
        }

        [DataMember(Name = "platform_id")]
        public int PlatformId { get; set; }

        [DataMember(Name = "platform_name")]
        public string PlatformName { get; set; }

        [DataMember(Name = "attributes")]
        public List<GameAttribute> Attributes { get; set; }

        [DataMember(Name = "patches")]
        public List<MobyGames.Entities.Patch> Patches { get; set; }

        [DataMember(Name = "releases")]
        public List<GameRelease> Releases { get; set; }

        public GameInformation()
        {
            _first_release_date = string.Empty;
        }
    }
}
