﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "cover")]
    public class Cover
    {

        [DataMember(Name = "comments")]
        public string Comments { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "height")]
        public int Height { get; set; }

        [DataMember(Name = "image")]
        public Uri Image { get; set; }

        [DataMember(Name = "scan_of")]
        public string ScanOf { get; set; }

        [DataMember(Name = "thumbnail_image")]
        public Uri ThumbnailImage { get; set; }

        [DataMember(Name = "width")]
        public int Width { get; set; }

        [DataMember(Name = "platforms")]
        public List<string> Plateforms { get; set; }

    }
}

