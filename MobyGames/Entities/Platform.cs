﻿using System;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "platform")]
    public class Platform
    {
        [DataMember(Name = "platform_id")]
        public int Id { get; set; }

        [DataMember(Name = "platform_name")]
        public string Name { get; set; }

    }
}
