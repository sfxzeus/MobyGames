﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MobyGames
{
    public static class GetArrayParameterExtension
    {
        public static string ToGetParameter(this int[] array, string parameterName)
        {
            /*string output = string.Empty;
            for (int i = 0; i < array.Length; i++)
            {
                output += (string.IsNullOrEmpty(output) ? "" : "&") + parameterName + "[" + array[i] + "]";
            }
            return output;*/
            return string.Join("&", array.Select((item, i) => System.Net.WebUtility.UrlEncode(parameterName + "[" + i + "]") + "=" + item));
        }
    }
}
