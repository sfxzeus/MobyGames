﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. 
 *
 * Written by Sylvain Laffont aka sfxzeus 
 * 
 * API documentation  :http://www.mobygames.com/info/api
 * 
 * MobyGames API Agreement - last updated January 2017. http://www.mobygames.com/api/agreement
 * - I agree that I will only use MobyGames data for non-commercial use
 *   If you have a commercial need, please contact Simon Carless & provide details on where you'd like to use the data. 
 * - I will not redistribute MobyGames data
 *   You agree to only use data from the API on the application, website, or use that you specified to us when originally applying to use the API. 
 * - I agree to provide attribution to MobyGames.com
 *   You must provide a credit/link back to MobyGames.com on any page or application that you use this data on. MobyGames relies on thousands of contributors and this helps us keep the ecosystem healthy. 
 * - I agree that MobyGames (Blue Flame Labs) is not liable for any element of my use
 *   Information is provided as is, with no warranty. We reserve the right to revoke access if we need to, though we appreciate you using it!
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using MobyGames.Entities;


namespace MobyGames
{
    public class MobyGamesClient
    {

        private const string _mobyGamesUri = "https://api.mobygames.com/v1/";
        private const string _mobyGameUserAgent = ".net core MobyGames client v1";
        private const string _genresEndpoint = "genres?api_key={0}";
        private const string _groupsEndpoint = "groups?api_key={0}";
        private const string _platformsEndpoint = "platforms?api_key={0}";
        private const string _gamesEndpoint = "games?api_key={0}&title={1}&format={2}&limit={3}&offset={4}";
        private const string _recentGamesEndpoint = "games/recent?api_key={0}&format={1}&limit={2}&offset={3}&age={4}";
        private const string _randomGamesEndpoint = "games/random?api_key={0}&format={1}&limit={2}";
        private const string _gameEndpoint = "games/{0}?api_key={1}&format={2}";
        private const string _gamePlatformsEndpoint = "games/{0}/platforms?api_key={1}";
        private const string _gamePlatformInformationEndpoint = "games/{0}/platforms/{1}?api_key={2}";
        private const string _gameScreenshotsEndpoint = "games/{0}/platforms/{1}/screenshots?api_key={2}";
        private const string _gameCoversEndpoint = "games/{0}/platforms/{1}/covers?api_key={2}";

        private Stack<DateTime> _requestTime;
        private readonly HttpClient _httpclient;
        private string _apiKey;

        /// <summary>
        /// Maximum request per minutes (Default 6) with a minimum of 1s between 2 requests
        /// If 0 desactivate the automatic dispatch control of request.
        /// </summary>
        public int MaxRequestPerMin { get; set; }

        public MobyGamesClient(string ApiKey)
        {

            _httpclient = new HttpClient() { BaseAddress = new Uri(_mobyGamesUri) };
            _httpclient.DefaultRequestHeaders.Add("user-agent", _mobyGameUserAgent);

            _apiKey = ApiKey;
            MaxRequestPerMin = 6;
            _requestTime = new Stack<DateTime>(6);
            _requestTime.Push(DateTime.Now);
        }


        #region Genres
        /// <summary>
        /// Provides a list of genres which may be used for filtering games 
        /// </summary>
        /// <returns>A list of genres</returns>
        public Genres Genres()
        {
            var task = GetData<Genres>(string.Format(_genresEndpoint, _apiKey));
            task.Wait();
            return task.Result;
        }

        /// <summary>
        /// Provides a list of genres which may be used for filtering games 
        /// </summary>
        /// <returns>A list of genres</returns>
        public async Task<Genres> GenresAsync()
        {
            return await GetData<Genres>(string.Format(_genresEndpoint, _apiKey));
        }
        #endregion

        #region Groups
        /// <summary>
        /// Provides a list of groups which may be used for filtering games
        /// </summary>
        /// <returns>A list of groups</returns>
        public Groups Groups()
        {
            var task = GetData<Groups>(string.Format(_groupsEndpoint, _apiKey));
            task.Wait();
            return task.Result;
        }

        /// <summary>
        /// Provides a list of groups which may be used for filtering games
        /// </summary>
        /// <returns>A list of groups</returns>
        public async Task<Groups> GroupsAsync()
        {
            return await GetData<Groups>(string.Format(_groupsEndpoint, _apiKey));
        }
        #endregion

        #region Platforms
        /// <summary>
        /// Provides a list of platforms which may be used for filtering games
        /// </summary>
        /// <returns>A list of platforms</returns>
        public Platforms Platforms()
        {
            var task = GetData<Platforms>(string.Format(_platformsEndpoint, _apiKey));
            task.Wait();
            return task.Result;
        }

        /// <summary>
        /// Provides a list of platforms which may be used for filtering games
        /// </summary>
        /// <returns>A list of platforms</returns>
        public async Task<Platforms> PlatformsAsync()
        {
            return await GetData<Platforms>(string.Format(_platformsEndpoint, _apiKey));
        }

        /// <summary>
        /// Provides a Lists platforms for which a game was released.
        /// </summary>
        /// <param name="id">game's id</param>
        /// <returns>A list of platforms</returns>
        public GamePlatforms Platforms(int id)
        {
            var task = GetData<GamePlatforms>(string.Format(_gamePlatformsEndpoint, id, _apiKey));
            task.Wait();
            return task.Result;
        }

        /// <summary>
        /// Provides a Lists platforms for which a game was released.
        /// </summary>
        /// <param name="id">game's id</param>
        /// <returns>A list of platforms</returns>
        public async Task<GamePlatforms> PlatformsAsync(int id)
        {
            return await GetData<GamePlatforms>(string.Format(_gamePlatformsEndpoint, id, _apiKey));
        }
        #endregion Platforms

        #region Game
        /// <summary>
        /// Provides information about a single game
        /// </summary>
        /// <param name="id">game's id</param>
        /// <param name="format">The output format: brief, normal or full</param>
        /// <returns>A game</returns>
        public Game Game(int id, GameFormat format)
        {
            try
            {
                var task = GetData<Game>(string.Format(_gameEndpoint, id, _apiKey, format.ToString()));
                task.Wait();
                return task.Result;
            }
            catch (Exception ex)
            {
                if (ex?.InnerException is MobyGamesException)
                    throw ex.InnerException;
                else
                    throw ex;
            }
        }

        /// <summary>
        /// Provides information about a single game
        /// </summary>
        /// <param name="id">game's id</param>
        /// <param name="format">The output format: brief, normal or full</param>
        /// <returns>A game</returns>
        public async Task<Game> GameAsync(int id, GameFormat format)
        {
            return await GetData<Game>(string.Format(_gameEndpoint, id, _apiKey, format.ToString()));
        }
        #endregion

        #region Games
        /// <summary>
        /// Provides a list of games matching the filters given order by id
        /// </summary>
        /// <param name="title">A substring of the title (not case sensitive)</param>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games</returns>
        public Games Games(string title, GameFormat format, int offset = 0, int limit = 100)
        {
            return Games(title, format, null, null, null, offset, limit);
        }

        /// <summary>
        /// Provides a list of games matching the filters given order by id
        /// </summary>
        /// <param name="title">A substring of the title (not case sensitive)</param>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games</returns>
        public async Task<Games> GamesAsync(string title, GameFormat format, int offset = 0, int limit = 100)
        {
            return await GamesAsync(title, format, null, null, null, offset, limit);
        }

        /// <summary>
        /// Provides a list of games matching the filters given order by id
        /// </summary>
        /// <param name="title">A substring of the title (not case sensitive)</param>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="platformIds">An array of platforms id</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games</returns>
        public Games Games(string title, GameFormat format, int[] platformIds, int offset = 0, int limit = 100)
        {
            return Games(title, format, platformIds, null, null, offset, limit);
        }

        /// <summary>
        /// Provides a list of games matching the filters given order by id
        /// </summary>
        /// <param name="title">A substring of the title (not case sensitive)</param>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="platformIds">An array of platforms id</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games</returns>
        public async Task<Games> GamesAsync(string title, GameFormat format, int[] platformIds, int offset = 0, int limit = 100)
        {
            return await GamesAsync(title, format, platformIds, null, null, offset, limit);
        }

        /// <summary>
        /// Provides a list of games matching the filters given order by id
        /// </summary>
        /// <param name="title">A substring of the title (not case sensitive)</param>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="platformIds">An array of platforms id</param>
        /// <param name="genreIds">An array of genres id</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games</returns>
        public Games Games(string title, GameFormat format, int[] platformIds, int[] genreIds, int offset = 0, int limit = 100)
        {
            return Games(title, format, platformIds, genreIds, null, offset, limit);
        }

        /// <summary>
        /// Provides a list of games matching the filters given order by id
        /// </summary>
        /// <param name="title">A substring of the title (not case sensitive)</param>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="platformIds">An array of platforms id</param>
        /// <param name="genreIds">An array of genres id</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games</returns>
        public async Task<Games> GamesAsync(string title, GameFormat format, int[] platformIds, int[] genreIds, int offset = 0, int limit = 100)
        {
            return await GamesAsync(title, format, platformIds, genreIds, null, offset, limit);
        }

        /// <summary>
        /// Provides a list of games matching the filters given order by id
        /// </summary>
        /// <param name="title">A substring of the title (not case sensitive)</param>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="platformIds">An array of platforms id</param>
        /// <param name="genreIds">An array of genres id</param>
        /// <param name="groupIds">An array of groups id</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games</returns>
        public Games Games(string title, GameFormat format, int[] platformIds, int[] genreIds, int[] groupIds, int offset = 0, int limit = 100)
        {
            string request = string.Format(_gamesEndpoint, _apiKey, System.Net.WebUtility.UrlEncode(title), format, limit, offset);
            if (platformIds?.Length > 0) request += "&" + platformIds.ToGetParameter("platform");
            if (genreIds?.Length > 0) request += "&" + genreIds.ToGetParameter("genre");
            if (groupIds?.Length > 0) request += "&" + groupIds.ToGetParameter("group");
            Games games;
            if (format == GameFormat.id)
            {
                games = new Games();
                var task = GetData<SimpleGames>(request);
                task.Wait();
                games.Add(task.Result.Games);
            }
            else
            {
                var task = GetData<Games>(request);
                task.Wait();
                games = task.Result;
            }

            return games;
        }

        /// <summary>
        /// Provides a list of games matching the filters given order by id
        /// </summary>
        /// <param name="title">A substring of the title (not case sensitive)</param>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="platformIds">An array of platforms id</param>
        /// <param name="genreIds">An array of genres id</param>
        /// <param name="groupIds">An array of groups id</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games</returns>
        public async Task<Games> GamesAsync(string title, GameFormat format, int[] platformIds, int[] genreIds, int[] groupIds, int offset = 0, int limit = 100)
        {
            string request = string.Format(_gamesEndpoint, _apiKey, System.Net.WebUtility.UrlEncode(title), offset, limit);
            if (platformIds?.Length > 0) request += "&" + platformIds.ToGetParameter("platform");
            if (genreIds?.Length > 0) request += "&" + genreIds.ToGetParameter("genre");
            if (groupIds?.Length > 0) request += "&" + groupIds.ToGetParameter("group");
            Games games;
            if (format == GameFormat.id)
            {
                games = await GetData<Games>(request);
            }
            else
            {
                games = new Games();
                games.Add(await GetData<int[]>(request));
            }
            return games;
        }
        #endregion

        #region Recent Games
        /// <summary>
        ///  Provides a list of games that have been modified recently
        /// </summary>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="age">Return only games modified in the last age days (max 21 days)</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games recently modified</returns>
        public Games RecentGames(GameFormat format, int age = 21, int offset = 0, int limit = 100)
        {
            string request = string.Format(_recentGamesEndpoint, _apiKey, format.ToString(), limit, offset, age);
            Games games;
            if (format == GameFormat.id)
            {
                games = new Games();
                var task = GetData<SimpleGames>(request);
                task.Wait();
                games.Add(task.Result.Games);

            }
            else
            {
                var task = GetData<Games>(request);
                task.Wait();
                games = task.Result;
            }
            return games;
        }

        /// <summary>
        ///  Provides a list of games that have been modified recently
        /// </summary>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="age">Return only games modified in the last age days (max 21 days)</param>
        /// <param name="offset">The offset from which to begin returning games (default 0)</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A list of games recently modified</returns>
        public async Task<Games> RecentGamesAsync(GameFormat format, int age = 21, int offset = 0, int limit = 100)
        {
            string request = string.Format(_recentGamesEndpoint, _apiKey, format.ToString(), limit, offset, age);
            Games games;
            if (format == GameFormat.id)
            {
                games = new Games();
                games.Add((await GetData<SimpleGames>(request)).Games);
            }
            else
            {
                games = await GetData<Games>(request);
            }
            return games;
        }
        #endregion

        #region Random Games
        /// <summary>
        ///  Provides a list of random games.It is recommended to cache the response for local use, rather than to call it repeatedly. This function will provide at most 100 random games, with the list being refreshed at most once per five minutes.
        /// </summary>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A random list of games</returns>
        public Games RandomGames(GameFormat format, int limit = 100)
        {
            string request = string.Format(string.Format(_randomGamesEndpoint, _apiKey, format.ToString(), limit));
            Games games;
            if (format == GameFormat.id)
            {
                games = new Games();
                var task = GetData<SimpleGames>(request);
                task.Wait();
                games.Add(task.Result.Games);
            }
            else
            {
                var task = GetData<Games>(request);
                task.Wait();
                games = task.Result;
            }
            return games;
        }

        /// <summary>
        ///  Provides a list of random games.It is recommended to cache the response for local use, rather than to call it repeatedly. This function will provide at most 100 random games, with the list being refreshed at most once per five minutes.
        /// </summary>
        /// <param name="format">The output format: id, brief, or normal</param>
        /// <param name="limit">The maximum number of games to return (default 100)</param>
        /// <returns>A random list of games</returns>
        public async Task<Games> RandomGamesAsync(GameFormat format, int limit = 100)
        {
            string request = string.Format(_randomGamesEndpoint, _apiKey, format.ToString(), limit);
            Games games;
            if (format == GameFormat.id)
            {
                games = new Games();
                games.Add((await GetData<SimpleGames>(request)).Games);
            }
            else
            {
                games = await GetData<Games>(request);

            }
            return games;
        }
        #endregion

        #region Screenshots
        /// <summary>
        /// Provides a list of screenshots for the specified game and platform
        /// </summary>
        /// <param name="gameId">game's id</param>
        /// <param name="platformId">platform's id</param>
        /// <returns>A list of sceenshots</returns>
        public Screenshots Screenshots(int gameId, int platformId)
        {
            var task = GetData<Screenshots>(string.Format(_gameScreenshotsEndpoint, gameId, platformId, _apiKey));
            task.Wait();
            return task.Result;
        }
        /// <summary>
        /// Provides a list of screenshots for the specified game and platform
        /// </summary>
        /// <param name="gameId">game's id</param>
        /// <param name="platformId">platform's id</param>
        /// <returns>A list of sceenshots</returns>
        public async Task<Screenshots> ScreenshotsAsync(int gameId, int platformId)
        {
            return await GetData<Screenshots>(string.Format(_gameScreenshotsEndpoint, gameId, platformId, _apiKey));
        }
        #endregion

        #region Covers
        /// <summary>
        /// Provides a list of cover groups for the specified game and platform.
        /// </summary>
        /// <param name="gameId">game's id</param>
        /// <param name="platformId">platform's id</param>
        /// <returns>A list of cover groups</returns>
        public CoverGroups Covers(int gameId, int platformId)
        {
            var task = GetData<CoverGroups>(string.Format(_gameCoversEndpoint, gameId, platformId, _apiKey));
            task.Wait();
            return task.Result;
        }

        public async Task<CoverGroups> CoversAsync(int gameId, int platformId)
        {
            return await GetData<CoverGroups>(string.Format(_gameCoversEndpoint, gameId, platformId, _apiKey));
        }
        #endregion

        #region Release Informations
        /// <summary>
        /// Provides a list information about a game's releases on the specified platform.
        /// </summary>
        /// <param name="gameId">game's id</param>
        /// <param name="platformId">platform's id</param>
        /// <returns>A list of information about a game's released on the specied platform</returns>
        public GameInformation ReleaseInformations(int gameId, int platformId)
        {
            var task = GetData<GameInformation>(string.Format(_gamePlatformInformationEndpoint, gameId, platformId, _apiKey));
            task.Wait();
            return task.Result;
        }

        /// <summary>
        /// Provides a list information about a game's releases on the specified platform. Asynchrone method.
        /// </summary>
        /// <param name="gameId">game's id</param>
        /// <param name="platformId">platform's id</param>
        /// <returns>A list of information about a game's released on the specied platform</returns>
        public async Task<GameInformation> ReleaseInformationsAsync(int gameId, int platformId)
        {
            return await GetData<GameInformation>(string.Format(_gamePlatformInformationEndpoint, gameId, platformId, _apiKey));
        }
        #endregion

        private async Task<T> GetData<T>(string url)
        {
            if (MaxRequestPerMin != 0)
            {
                TimeSpan lastCallSpan = DateTime.Now - _requestTime.Peek();
                if (lastCallSpan.Milliseconds < 1000)
                {
                    Task.Delay(1000 - lastCallSpan.Milliseconds).Wait();
                }

            }
            using (var request = await _httpclient.GetAsync(url))
            {
                using (var stream = await request.Content.ReadAsStreamAsync())
                {

                    _requestTime.Push(DateTime.Now);
                    if (stream == null)
                        return default(T);

                    /*var memoryStream = new MemoryStream();
                    await stream.CopyToAsync(memoryStream);
                    string debugStr = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray(),0,(int)memoryStream.Length);*/

                    switch (request.StatusCode)
                    {
                        case System.Net.HttpStatusCode.BadRequest:
                            throw new BadRequestException(GetObjetFromStream<MobyGamesError>(stream));
                        case System.Net.HttpStatusCode.Unauthorized:
                            throw new UnauthorizedException(GetObjetFromStream<MobyGamesError>(stream));
                        case System.Net.HttpStatusCode.NotFound:
                            throw new NotFoundException(GetObjetFromStream<MobyGamesError>(stream));
                        case (System.Net.HttpStatusCode)422:
                            throw new UnprocessableEntityException(GetObjetFromStream<MobyGamesError>(stream));
                        case (System.Net.HttpStatusCode)429:
                            throw new TooManyRequestsException(GetObjetFromStream<MobyGamesError>(stream));
                    }

                    return GetObjetFromStream<T>(stream);
                }
            }

        }

        private T GetObjetFromStream<T>(Stream stream)
        {
            var dcs = new DataContractJsonSerializer(typeof(T));
            return (T)dcs.ReadObject(stream);
        }

        public MobyGamesError TestError()
        {
            var task = GetData<MobyGamesError>("error.json");
            task.Wait();
            return task.Result;
        }
    }
}
