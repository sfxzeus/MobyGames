﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "games")]
    public class Games : IEnumerable<Game>
    {
        [DataMember(Name = "games")]
        private List<Game> _games { get; set; }

        public Game this[int index]
        {
            get { return _games[index]; }
            set { _games.Insert(index, value); }
        }

        public int Count { get { return _games.Count; } }

        public IEnumerator<Game> GetEnumerator()
        {
            return _games.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void Add(int[] gamesId)
        {
            if (_games == null) _games = new List<Game>();

            foreach (int id in gamesId)
            {
                _games.Add(new Game() { Id = id });
            }
        }
    }
}
