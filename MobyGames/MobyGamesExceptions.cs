﻿using System;
using MobyGames.Entities;

namespace MobyGames
{
    public class MobyGamesException : Exception
    {
        public MobyGamesError Error { get; internal set; }
        public MobyGamesException(MobyGamesError error) : base(error == null ? "Failure" : error.Error)
        {
            Error = error;
        }
    }

    public class TooManyRequestsException : MobyGamesException
    {
        public TooManyRequestsException(MobyGamesError error) : base(error) { }
    }
    public class UnprocessableEntityException : MobyGamesException
    {
        public UnprocessableEntityException(MobyGamesError error) : base(error) { }
    }
    public class NotFoundException : MobyGamesException
    {
        public NotFoundException(MobyGamesError error) : base(error) { }
    }
    public class UnauthorizedException : MobyGamesException
    {
        public UnauthorizedException(MobyGamesError error) : base(error) { }
    }
    public class BadRequestException : MobyGamesException {
        public BadRequestException(MobyGamesError error) : base(error) { }
    }

}
