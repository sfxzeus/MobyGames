﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "platforms")]
    public class Platforms : IEnumerable<Platform>
    {
        [DataMember(Name = "platforms")]
        private List<Platform> _platforms { get; set; }

        public Platform this[int index]
        {
            get { return _platforms[index]; }
            set { _platforms.Insert(index, value); }
        }

        public int Count { get { return _platforms.Count; } }

        public IEnumerator<Platform> GetEnumerator()
        {
            return _platforms.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
