﻿using System;
using MobyGames;
using MobyGames.Entities;
using System.ComponentModel;


namespace MobyGamesTestnet45
{
    class Program
    {
        static void Main(string[] args)
        {

            MobyGamesClient mg = new MobyGamesClient(System.IO.File.ReadAllText("key.txt"));
            /*
            Console.WriteLine("---------- Genres ----------");
            foreach (var genre in mg.Genres())
            {
                PrintObject(genre);
            }
            Console.WriteLine("\r---------- Groups ----------");
            foreach (var group in mg.Groups())
            {
                PrintObject(group);
            }

            Console.WriteLine("\r---------- Platforms ----------");
            foreach (var platform in mg.Platforms())
            {
                PrintObject(platform);
            }

            Console.WriteLine("\r---------- Recent Games ID ----------");
            foreach (var game in mg.RecentGames(GameFormat.id))
            {
                Console.Write(game.Id + ",");
            }

            Console.WriteLine("\r---------- Recent Games Brief ----------");
            foreach (var game in mg.RecentGames(GameFormat.brief, 21, 0, 10))
            {
                Console.WriteLine("[");
                Console.WriteLine("Id=" + game.Id);
                Console.WriteLine("Title=" + game.Title);
                Console.WriteLine("Url=" + game.Url);
                Console.Write("]");
            }

            Console.WriteLine("\r---------- Recent Games Normal ----------");
            foreach (var game in mg.RecentGames(GameFormat.normal, 21, 0, 2))
            {
                PrintObject(game);
            }

            Console.WriteLine("\r---------- Random Games Id ----------");
            foreach (var game in mg.RandomGames(GameFormat.id))
            {
                Console.Write(game.Id + ",");
            }
            Console.WriteLine();
            Console.WriteLine("\r---------- Random Games Brief ----------");
            foreach (var game in mg.RandomGames(GameFormat.brief, 10))
            {
                Console.WriteLine("[");
                Console.WriteLine("Id=" + game.Id);
                Console.WriteLine("Title=" + game.Title);
                Console.WriteLine("Url=" + game.Url);
                Console.Write("]");
            }

            Console.WriteLine("\r---------- Random Games Normal ----------");
            foreach (var game in mg.RandomGames(GameFormat.normal, 2))
            {
                PrintObject(game);
            }

            Console.WriteLine("\r---------- Games Id with name Mario ----------");
            foreach (var game in mg.Games("mario", GameFormat.id))
            {
                Console.Write(game.Id + ",");
            }
            Console.WriteLine();

            Console.WriteLine("\r---------- Games brief with name Mario ----------");
            foreach (var game in mg.Games("mario", GameFormat.brief, 0, 5))
            {
                Console.WriteLine("[");
                Console.WriteLine("Id=" + game.Id);
                Console.WriteLine("Title=" + game.Title);
                Console.WriteLine("Url=" + game.Url);
                Console.Write("]");
            }
            Console.WriteLine();

            Console.WriteLine("\r---------- Games Normal with name Mario ----------");
            foreach (var game in mg.Games("mario", GameFormat.normal, 0, 2))
            {
                PrintObject(game);
            }
            Console.WriteLine();

            Console.WriteLine("\r---------- Games Normal with genre Perspective named Mario ----------");
            foreach (var game in mg.Games("mario", GameFormat.normal, null, new int[] { 126 }))
            {
                PrintObject(game);
            }
            Console.WriteLine();
            
            Console.WriteLine("\r---------- Get Game by Id (titanfall) ----------");
            PrintObject(mg.Game(64417, GameFormat.brief));
            PrintObject(mg.Game(64417, GameFormat.normal));

            Console.WriteLine();
            Console.WriteLine("\r---------- Get Game Covers by Id (titanfall) and platform (windows)----------");
            foreach (var cover in mg.Covers(64417,3))
            {
                PrintObject(cover);
            }


            Console.WriteLine();
            Console.WriteLine("\r---------- Get Game Screenshots by Id (titanfall) and platform (windows)----------");
            foreach (var screenshot in mg.Screenshots(64417, 3))
            {
                PrintObject(screenshot);
            }

            Console.WriteLine();
            Console.WriteLine("\r---------- Get Game platforms by Id (titanfall) ----------");
            foreach (var platform in mg.Platforms(64417))
            {
                PrintObject(platform);
            }*/

            GameInformation gi = mg.ReleaseInformations(5451, 3);
            PrintObject(gi);
            Console.ReadLine();
        }

        private static void PrintObject(object o)
        {
            Console.WriteLine("[");
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(o))
            {
                string name = descriptor.Name;
                object value = descriptor.GetValue(o);
                Console.WriteLine("{0}={1}", name, value);
            }
            Console.Write("]");
        }
    }
}
