﻿using System;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "platform")]
    public class GamePlatform
    {
        [DataMember(Name = "platform_id")]
        public int Id { get; set; }

        [DataMember(Name = "platform_name")]
        public string Name { get; set; }

        [DataMember(Name = "first_release_date")]
        private string _first_release_date;

        public DateTime FirstReleaseDate
        {
            get
            {
                if (_first_release_date.Length == 4)
                {
                    return new DateTime(int.Parse(_first_release_date), 1, 1);
                }
                else
                {
                    return DateTime.Parse(_first_release_date);
                }
            }
        }
        public GamePlatform()
        {
            _first_release_date = string.Empty;
        }
    }
}
