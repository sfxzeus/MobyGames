﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "cover_groups")]
    public class CoverGroups : IEnumerable<CoverGroup>
    {
        [DataMember(Name = "cover_groups")]
        private List<CoverGroup> _covergroups { get; set; }

        public CoverGroup this[int index]
        {
            get { return _covergroups[index]; }
            set { _covergroups.Insert(index, value); }
        }

        public int Count { get { return _covergroups.Count; } }

        public IEnumerator<CoverGroup> GetEnumerator()
        {
            return _covergroups.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
