﻿using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "error")]
    public class MobyGamesError
    {
        [DataMember(Name = "code")]
        public int Code { get; set; }

        [DataMember(Name = "error")]
        public string Error { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}
/*
{
  "code": 1, 
  "error": "Game not found", 
  "message": ""
}
*/
