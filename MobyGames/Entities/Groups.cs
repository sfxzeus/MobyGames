﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "groups")]
    public class Groups : IEnumerable<Group>
    {
        [DataMember(Name = "groups")]
        private List<Group> _groups { get; set; }

        public Group this[int index]
        {
            get { return _groups[index]; }
            set { _groups.Insert(index, value); }
        }

        public int Count { get { return _groups.Count; } }

        public IEnumerator<Group> GetEnumerator()
        {
            return _groups.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
