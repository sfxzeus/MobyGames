﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "genres")]
    public class Genres : IEnumerable<Genre>
    {
        [DataMember(Name = "genres")]
        private List<Genre> _genres { get; set; }

        public Genre this[int index]
        {
            get { return _genres[index]; }
            set { _genres.Insert(index, value); }
        }

        public int Count { get { return _genres.Count; } }

        public IEnumerator<Genre> GetEnumerator()
        {
            return _genres.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
