﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MobyGames.Entities
{
    [DataContract(Name = "game")]
    public class Game
    {
        [DataMember(Name = "game_id")]
        public int Id { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name ="genres")]
        public List<Genre> Genres { get; set; }

        [DataMember(Name = "moby_score")]
        public float Score { get; set; }

        [DataMember(Name = "moby_url")]
        public Uri Url { get; set; }

        [DataMember(Name = "num_votes")]
        public int VotesCount { get; set; }

        [DataMember(Name = "official_url")]
        public Uri OfficialUrl { get; set; }

        [DataMember(Name = "platforms")]
        public List<GamePlatform> Platforms { get; set; }

        [DataMember(Name = "sample_cover")]
        public Cover SampleCover { get; set; }

        [DataMember(Name = "sample_screenshots")]
        public List<Screenshot> Screenshots { get; set; }

        [DataMember(Name ="title")]
        public string Title { get; set; }
    }
}
